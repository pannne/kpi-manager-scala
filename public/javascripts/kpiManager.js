function addServices(contentId, packageId) {
    $('#'+contentId).append('<div class="table-responsive"><table class="table table-striped"><tbody>');
    //$('#'+contentId).append('<tr><td>'+$(this).val()+'</td></tr>');
    var servicesArray = JSON.parse($('#servicesJson').val());

    $.each(servicesArray.services, function(index, val) {
        if(packageId == val.packageId) {
            $('#'+contentId).append('<tr><td><label for="'+val.serviceName+'">'+val.serviceName+'</label></td><td><input type="text" id="' + val.serviceName + '" maxlength="2" style="width: 30px;"></td></tr>');
            //$('#'+contentId).append('<td><input type="text" id="' + val.serviceName + '" maxlength="2"></td></tr>');
        }
    });
    $('#'+contentId).append('</tbody></table></div>');
}

function addPackages(contentId, boxId) {
    var arr = JSON.parse($('#boxJson').val());
    $.each(arr.boxPackage, function(index, val) {
        if(boxId == val.boxId) {
            addServices(contentId, val.packageId);
        }
    });
}