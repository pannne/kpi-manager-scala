package controllers

import java.util.Calendar
import javax.inject._

import play.api._
import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc.Flash
import play.api.Play.current
import play.api.i18n.Messages.Implicits._
import models._
import play.api.libs.json.JsValue

@Singleton
class UserController @Inject() extends Controller {

  private val userForm: Form[User] = Form(
    mapping(
      "id" -> optional(longNumber),
      "username" -> nonEmptyText,
      "pass" -> nonEmptyText,
      "email" -> email,
      "createdBy" -> optional(text),
      "createdOn" -> optional(sqlDate),
      "lastLogin" -> optional(sqlDate),
      "roleId" -> longNumber,
      "activeUser" -> optional(boolean),
      "emailConfirmed" -> ignored(false),
      "firstName" -> nonEmptyText,
      "lastName" -> nonEmptyText
    )(User.apply)(User.unapply)
  )

  def listAll = Action { implicit request =>
    val users = UserDAO.getAll
    Ok(views.html.users.userList(users))
  }

  def showDetails(name: String) = Action { implicit request =>

    val user = UserDAO.getByUsername(name) //.map {
    //	user => Ok(views.html.users.details(user))
    //}.getOrElse(NotFound)
    Ok(views.html.users.details(user))
  }

  def newUser = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      userForm.bind(request.flash.data)
    else userForm
    //		Redirect(routes.UserController.listAll)
    Ok(views.html.users.newUser(form))
  }

  def save = Action { implicit request =>
    val newUserForm = userForm.bindFromRequest()

    newUserForm.fold(
      hasErrors = { form =>
      val err = form.errorsAsJson
      Redirect(routes.UserController.newUser()).flashing(Flash(form.data) + ("error" -> err.toString))
    },
      success = { newUser =>
      UserDAO.addUser(newUser)
      val historyRecord: UserHistory = new UserHistory(None, 1, "New user created", new java.sql.Date(Calendar.getInstance().getTime().getTime()))
      UserHistoryDAO.insert(historyRecord)
      Redirect(routes.UserController.showDetails(newUser.username))
    }
    )
  }
}
