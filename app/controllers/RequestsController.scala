package controllers

import javax.inject._

import play.api.mvc.Flash
import play.api.i18n.{ I18nSupport, MessagesApi }
import utils.silhouette.AuthController
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{ Clock, PasswordHasherRegistry }
import com.mohiva.play.silhouette.api._
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import models._
import play.api._
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import utils.Mailer
import utils.silhouette._

@Singleton
class RequestsController @Inject() (
    val silhouette: Silhouette[MyEnv],
    val messagesApi: MessagesApi,
    userService: UserService,
    authInfoRepository: AuthInfoRepository,
    credentialsProvider: CredentialsProvider,
    tokenService: MailTokenService[MailTokenUser],
    passwordHasherRegistry: PasswordHasherRegistry,
    mailer: Mailer,
    conf: Configuration,
    clock: Clock
) extends AuthController with I18nSupport {

  private val requestForm: Form[models.Request] = Form(
    mapping(
      "id" -> optional(longNumber),
      "companyPIB" -> text,
      "ownerId" -> longNumber,
      "sellerId" -> optional(longNumber),
      "statusId" -> longNumber,
      "created" -> optional(sqlDate),
      "description" -> nonEmptyText,
      "serviceQuantity" -> text
    )(models.Request.apply)(models.Request.unapply)
  )

  def newRequest(id: Long) = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) =>
        Logger.logger.debug("newRequest: User found : " + user.email)
        val form = if (request.flash.get("error").isDefined)
          requestForm.bind(request.flash.data)
        else requestForm
        val servicesList = ServiceDAO.getAllForPackage(id)
        Ok(views.html.requests.newRequest(form, servicesList, 0.toLong))
      case None => Redirect(routes.Auth.signIn())
    }
  }

  def newBoxRequest(id: Long) = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) =>
        val form = if (request.flash.get("error").isDefined)
          requestForm.bind(request.flash.data)
        else requestForm
        val boxPkgList = ServicePackageDAO.getAllBoxPackages(id)
        var servicesList: List[Service] = Nil
        for (pkg <- boxPkgList)
          servicesList = servicesList ++ ServiceDAO.getAllForPackage(pkg.packageId)
        Ok(views.html.requests.newRequest(form, servicesList, id))
      case None => Redirect(routes.Auth.signIn())
    }
  }

  def create(packageId: Long) = SecuredAction { implicit request =>

    Logger.logger.debug("Request CREATE called : ")
    val newRequestForm = requestForm.bindFromRequest()

    newRequestForm.fold(
      hasErrors = {
      form =>
        val err = form.errorsAsJson
        Redirect(routes.RequestsController.listAll()).flashing(Flash(form.data) + ("error" -> err.toString))
    },
      success = {
      newRequest =>
        // Logger.logger.debug("Create new request : " + user.email)
        RequestDAO.insertRequest(newRequest)
        Redirect(routes.RequestsController.listAll())
    }
    )
  }

  def details(id: Long) = Action { implicit request =>

    val requestR = RequestDAO.getById(id.toInt)
    val requestHistory: List[RequestHistory] = RequestHistoryDAO.getRequestHistory(id)
    val serviceRequest: List[RequestService] = RequestServiceDAO.getByRequest(id)

    Ok(views.html.requests.details(requestR, requestHistory, serviceRequest))
  }

  def listAll = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) =>
        val requests = models.RequestDAO.getAll
        Ok(views.html.requests.list(requests))
      // TODO: case None
    }

  }

  def filter(filterStr: String) = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) =>
        val requests = models.RequestDAO.getByCompany(filterStr)
        Ok(views.html.requests.list(requests))
    }
  }

  def listUsersRequests() = UserAwareAction {
    implicit request =>
      request.identity match {
        case Some(user) =>
          val requests = models.RequestDAO.getByOwner(user.id.get)
          Ok(views.html.requests.list(requests))
        case None => Redirect(routes.Auth.signIn())
      }
  }
}
