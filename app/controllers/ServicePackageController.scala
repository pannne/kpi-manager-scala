package controllers

import models.{ Service, ServiceDAO, ServicePackage, ServicePackageDAO, BoxPackage }
import javax.inject.{ Inject, Singleton }
import play.api.data.Form
import play.api.data.Forms.{ longNumber, mapping, nonEmptyText, optional, text }
import play.api.i18n.{ I18nSupport, MessagesApi }
import play.api.mvc.{ Action, Controller }

/**
 * Created by darko on 6/20/17.
 */
@Singleton
class ServicePackageController @Inject() (val messagesApi: MessagesApi) extends Controller with I18nSupport {

  private val serviceForm: Form[Service] = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "packageId" -> longNumber,
      "description" -> optional(text)
    )(Service.apply)(Service.unapply)
  )

  private val packageForm: Form[ServicePackage] = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "description" -> optional(text)
    )(ServicePackage.apply)(ServicePackage.unapply)
  )

  private val boxForm: Form[BoxPackage] = Form(
    mapping(
      "id" -> optional(longNumber),
      "name" -> nonEmptyText,
      "description" -> optional(text),
      "pkgIds" -> optional(text)
    )(BoxPackage.apply)(BoxPackage.unapply)
  )

  def showDetails(id: Long) = Action { implicit request =>
    val servicePkg = ServicePackageDAO.getById(id)
    val servicesList = ServiceDAO.getAllForPackage(id)
    Ok(views.html.services.list(servicePkg, servicesList))
  }

  def serviceDetails(id: Long) = Action { implicit request =>
    NotImplemented
  }

  def serviceUpdate(id: Long) = Action { implicit request =>
    NotImplemented
  }

  def newService = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      serviceForm.bind(request.flash.data)
    else serviceForm
    Ok(views.html.services.newService(form))
  }

  def createService = Action { implicit request =>
    val newServiceForm = serviceForm.bindFromRequest()

    newServiceForm.fold(
      hasErrors = { form => Redirect(routes.RequestsController.listAll()) },
      success = { newService =>
      ServiceDAO.createService(newService)
      Redirect(routes.RequestsController.listAll())
    }
    )
  }

  def newPackage() = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      packageForm.bind(request.flash.data)
    else packageForm
    Ok(views.html.services.newPackage(form))
  }

  def createPackage = Action { implicit request =>
    val newPackageForm = packageForm.bindFromRequest()

    newPackageForm.fold(
      hasErrors = { form => Redirect(routes.RequestsController.listAll()) },
      success = { newPackage =>
      ServicePackageDAO.createPackage(newPackage)
      Redirect(routes.RequestsController.listAll())
    }
    )
  }

  def newBox = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      boxForm.bind(request.flash.data)
    else boxForm

    val packageList: List[ServicePackage] = ServicePackageDAO.getAll
    Ok(views.html.services.newBoxPackage(form, packageList))
  }

  def createBox() = Action { implicit request =>
    val newBoxForm = boxForm.bindFromRequest()
    newBoxForm.fold(
      hasErrors = { form => Redirect(routes.RequestsController.listAll()) },
      success = { newBox =>
      ServicePackageDAO.createBox(newBox)
      Redirect(routes.RequestsController.listAll())
    }
    )
  }
}
