package controllers

import javax.inject._

import play.api._
import play.api.mvc._
import play.api.data.Form
import play.api.data.Forms.{ longNumber, mapping, nonEmptyText, optional, text }
import models.Company
import utils.silhouette._
import models._
import play.api.i18n.I18nSupport
import java.util.concurrent.TimeoutException

import akka.actor.Status.Success
import com.mohiva.play.silhouette.api.Silhouette
import com.mohiva.play.silhouette.api.repositories.AuthInfoRepository
import com.mohiva.play.silhouette.api.util.{ Clock, PasswordHasherRegistry }
import com.mohiva.play.silhouette.impl.providers.CredentialsProvider
import play.api.i18n.MessagesApi
import utils.Mailer

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@Singleton
class CompanyController @Inject() (
    val silhouette: Silhouette[MyEnv],
    val messagesApi: MessagesApi,
    userService: UserService,
    authInfoRepository: AuthInfoRepository,
    credentialsProvider: CredentialsProvider,
    tokenService: MailTokenService[MailTokenUser],
    passwordHasherRegistry: PasswordHasherRegistry,
    mailer: Mailer,
    conf: Configuration,
    clock: Clock
) extends AuthController with I18nSupport {

  private val companyForm: Form[Company] = Form(
    mapping(
      "id" -> optional(longNumber), // .verifying("Duplicate id", CompanyDAO.getById(_).value.get.isEmpty),
      "pib" -> nonEmptyText,
      "name" -> nonEmptyText,
      "tisId" -> optional(text),
      "zr" -> optional(text)
    )(Company.apply)(Company.unapply)
  )

  def listAll = UserAwareAction { implicit request =>
    request.identity match {
      case Some(user) => Ok(views.html.companies.list(CompanyDAO.getAll))

      //        CompanyDAO.findAll.map {
      //          case companies => Ok(views.html.companies.list(companies))
      //
      //        }.recover {
      //          case ex: TimeoutException =>
      //            Logger.error("DB timeout occurred")
      //            InternalServerError(ex.getMessage)
      //        }.onComplete{
      //          case companies => Ok(views.html.companies.list(companies))
      //        }

      case None => Redirect(routes.Auth.signIn())
    }
  }

  def newCompany = Action { implicit request =>
    val form = if (request.flash.get("error").isDefined)
      companyForm.bind(request.flash.data)
    else companyForm
    Ok(views.html.companies.newCompany(form))
  }

  def save = Action { implicit request =>
    val newCompanyForm = companyForm.bindFromRequest()

    newCompanyForm.fold(
      hasErrors = { form => Redirect(routes.CompanyController.listAll) },
      success = { newCompany =>
      CompanyDAO.createCompany(newCompany)
      Redirect(routes.CompanyController.listAll)
    }
    )
  }

  def filter(pib: String) = UserAwareAction.async { implicit request =>
    request.identity match {
      case Some(user) => CompanyDAO.findByPIB(pib).map {
        case companies => Ok(views.html.companies.list(companies))
      }.recover {
        case ex: TimeoutException =>
          Logger.error("Problem found in employee list process")
          InternalServerError(ex.getMessage)
      }
    }
  }
}
