package controllers

import javax.inject.{ Inject, Singleton }

import com.mohiva.play.silhouette.api.Silhouette
import play.api._
import play.api.i18n.{ Lang, MessagesApi }
import utils.silhouette._

import models.Roles

@Singleton
class Application @Inject() (val silhouette: Silhouette[MyEnv], val messagesApi: MessagesApi) extends AuthController {

  def index = UserAwareAction { implicit request =>
    Ok(views.html.index())
  }

  def myAccount = SecuredAction { implicit request =>
    Ok(views.html.myAccount())
  }

  // REQUIRED ROLES: Support or admin
  def serviceSupport = SecuredAction(WithService(Roles.ROLE_ADMIN, Roles.ROLE_SUPPORT)) { implicit request =>
    Ok(views.html.serviceA())
  }

  // REQUIRED ROLES: Seller or admin
  def serviceSeller = SecuredAction(WithService(Roles.ROLE_ADMIN, Roles.ROLE_SELLER)) { implicit request =>
    Ok(views.html.serviceAorServiceB())
  }

  // REQUIRED ROLES: serviceA AND serviceB (or master)
  //  def serviceAandServiceB = SecuredAction(WithServices("serviceA", "serviceB")) { implicit request =>
  //    Ok(views.html.serviceAandServiceB())
  //  }

  // REQUIRED ROLES: master
  def serviceAdmin = SecuredAction(WithService(Roles.ROLE_ADMIN)) { implicit request =>
    Ok(views.html.settings())
  }

  def selectLang(lang: String) = UserAwareAction { implicit request =>
    Logger.logger.debug("Change user lang to : " + lang)
    request.headers.get(REFERER).map { referer =>
      Redirect(referer).withLang(Lang(lang))
    }.getOrElse {
      Redirect(routes.Application.index).withLang(Lang(lang))
    }
  }

}