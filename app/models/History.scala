package models

import java.sql.Date
import play.api.db.slick.DatabaseConfigProvider
import play.api.Play
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

/**
 * Created by darko on 7/3/17.
 */
case class UserHistory(id: Option[Long], userId: Long, comment: String, created: Date)

class UserHistoryTable(tag: Tag) extends Table[UserHistory](tag, "user_history") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def userId = column[Long]("user_id")
  def comment = column[String]("comment")
  def created = column[Date]("created_on")

  // Constraints
  def fk_user = foreignKey("USER_FK", userId, UserDAO.users)(_.id)

  def * = (id.?, userId, comment, created) <> (UserHistory.tupled, UserHistory.unapply)
}

object UserHistoryDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val history = TableQuery[UserHistoryTable]

  private def filterByUser(userId: Long): Query[UserHistoryTable, UserHistory, List] = history.filter(_.userId === userId).to[List]

  def getUserHistory(userId: Long): List[UserHistory] = Await.result(db.run(filterByUser(userId).result), 5.seconds)
  def insert(historyRecord: UserHistory): Future[Int] = db.run(history += historyRecord)

}

case class RequestHistory(id: Option[Long], requestId: Long, comment: String, created: Date)

class RequestHistoryTable(tag: Tag) extends Table[RequestHistory](tag, "request_history") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def requestId = column[Long]("request_id")
  def comment = column[String]("comment")
  def created = column[Date]("created_on")

  // TODO: Constraints
  //def fkRequest =

  def * = (id.?, requestId, comment, created) <> (RequestHistory.tupled, RequestHistory.unapply)
}

object RequestHistoryDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val history = TableQuery[RequestHistoryTable]

  private def filterByRequest(requestId: Long): Query[RequestHistoryTable, RequestHistory, List] = history.filter(_.requestId === requestId).to[List]

  def getRequestHistory(requestId: Long): List[RequestHistory] = Await.result(db.run(filterByRequest(requestId).result), 5.seconds)
  def insert(historyRecord: RequestHistory): Future[Int] = db.run(history += historyRecord)
}