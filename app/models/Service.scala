package models

import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.json.{ Json, JsObject }
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.concurrent.Future

/**
 * Created by darko on 6/20/17.
 */
case class Service(id: Option[Long], serviceName: String, packageId: Long, description: Option[String])

class ServiceTable(tag: Tag) extends Table[Service](tag, "service") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def packageId = column[Long]("service_package_id")
  def description = column[String]("description")

  def service_package_id = foreignKey("PACKAGE_FK", packageId, ServicePackageDAO.servicePackages)(_.id)

  def * = (id.?, name, packageId, description.?) <> (Service.tupled, Service.unapply)
}

object ServiceDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val services = TableQuery[ServiceTable]

  private def filterByPackage(packageId: Long): Query[ServiceTable, Service, List] =
    services.filter(_.packageId === packageId).to[List]

  def getAllForPackage(id: Long): List[Service] = Await.result(db.run(filterByPackage(id).result), 5.seconds)

  def createService(service: Service): Future[Int] = db.run(services += service)
  def getAllServices(): List[Service] = Await.result(db.run(services.to[List].result), 5.seconds)

  /**
   * Implicit format object
   */
  implicit val serviceFormat = Json.format[Service]

  def getJson(): String = Json.obj("services" -> getAllServices()).toString()

}

/*
object Service {

  var dummyServices = Set(
    Service(Some(1L), "Nov", Some(1L)),
    Service(Some(2L), "Seoba", Some(1L)),
    Service(Some(3L), "Nov POTS", Some(2L)),
    Service(Some(4L), "Nov BBTF", Some(2L)),
    Service(Some(5L), "Postojeca linija", Some(2L)),
    Service(Some(6L), "Ustupanje", Some(2L)),
    Service(Some(7L), "Seoba", Some(2L)),
    Service(Some(8L), "Zamena ISDN", Some(2L)),
    Service(Some(9L), "Nov", Some(3L)),
    Service(Some(10L), "PK/PB", Some(3L)),
    Service(Some(11L), "Statika", Some(3L)),
    Service(Some(12L), "Seoba", Some(3L)),
    Service(Some(13L), "DEM", Some(3L)),
    Service(Some(14L), "Tablet", Some(3L)),
    Service(Some(15L), "Nov", Some(4L)),
    Service(Some(16L), "Seoba", Some(4L)),
    Service(Some(17L), "DEM", Some(4L)),
    Service(Some(18L), "Nova", Some(5L)),
    Service(Some(19L), "Integracija", Some(5L)),
    Service(Some(20L), "MNP", Some(5L)),
    Service(Some(21L), "Postojeca biz tarifa", Some(5L)),
    Service(Some(22L), "Ustupanje", Some(5L)),
    Service(Some(23L), "Promena tarife", Some(5L)),
    Service(Some(24L), "Zamena kartice", Some(5L)),
    Service(Some(25L), "Dodatne usluge", Some(5L)),
    Service(Some(26L), "DEM", Some(5L)),
    Service(Some(27L), "Izdavanje mobilnih aparata", Some(5L)),
    Service(Some(28L), "Nov", Some(6L)),
    Service(Some(29L), "Ustupanje", Some(6L)),
    Service(Some(30L), "DEM", Some(6L))
  )

  def getAllForPackage(id: Long) = dummyServices.filter(_.packageId.get == id).toList.sortBy(_.id)
}
*/ 