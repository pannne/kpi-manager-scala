package models

import scala.concurrent.{ ExecutionContext }
import javax.inject.Inject

import play.api.db.slick.DatabaseConfigProvider
import play.api.Play
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

/**
 * Company model class
 * @param id
 * @param pib
 * @param name
 * @param tisId
 * @param zr
 */
case class Company(id: Option[Long], pib: String, name: String, tisId: Option[String], zr: Option[String])

class CompanyTable(tag: Tag) extends Table[Company](tag, "company") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def pib = column[String]("pib")
  def name = column[String]("name")
  def tisId = column[String]("tis_id")
  def zr = column[String]("zr")

  def * = (id.?, pib, name, tisId.?, zr.?) <> (Company.tupled, Company.unapply _)
}

object CompanyDAO {

  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val companies = TableQuery[CompanyTable]

  private def filterById(id: Long): Query[CompanyTable, Company, Seq] = companies.filter(_.id === id)

  private def filterByName(name: String): Query[CompanyTable, Company, List] = {
    val withWildcards = "%" + name + "%"
    companies.filter(_.name like withWildcards).to[List]
  }

  def filterByPIB(pib: String): Query[CompanyTable, Company, List] = {
    val withWildcards = "%" + pib + "%"
    companies.filter(_.pib like withWildcards).to[List]
  }

  def findAll: Future[List[Company]] = db.run(companies.to[List].result) // companies.toList.sortBy(_.id)
  def getAll: List[Company] = Await.result(findAll, 5.seconds)

  def getById(id: Long): Future[Company] = {
    val result = db.run(filterById(id).result.head) // companies.find( _.id == id).get
    result
    // def findByID(id: Long) = companies.find( _.id == id )
  }

  def findByName(name: String): Future[List[Company]] = db.run(filterByName(name).result)
  def findByPIB(pib: String): Future[List[Company]] = db.run(filterByPIB(pib).result)

  def getByPIB(pib: String): Company = Await.result(db.run(filterByPIB(pib).result.head), 5.seconds)

  def createCompany(company: Company): Future[Int] = {
    //companies = companies + company
    db.run(companies += company)
  }

  /**
   * Dummy data
   * var companies = Set(
   * Company(1, "106475837", "EXPERIENS PLUS DOO BEOGRAD", "5453254"),
   * Company(2, "100760118", "PODMLADAK DOO", "2926281"),
   * Company(3, "100391986", "OMLADINSKI KAMP DJERDAP", "847266"),
   * Company(4, "102685624", "ASTRA COOL", "3027139"),
   * Company(5, "104031026", "DRVOIMPEX INZENJERING", "5557278"),
   * Company(6, "102201098", "MNEM SYNE", "891829"),
   * Company(7, "109474676", "FURNIX", "20250472"),
   * Company(8, "108852950", "CABLENETWORK ENGINEERING", "")
   * )
   */
}
