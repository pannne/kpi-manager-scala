package models;

import play.api.db.slick.DatabaseConfigProvider
import play.api.Play
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

case class Role(id: Option[Long], name: String)

object Roles {
  val ROLE_SYSTEM = 1L
  val ROLE_ADMIN = 2L
  val ROLE_SUPPORT = 3L
  val ROLE_SELLER = 4L
}

class RoleTable(tag: Tag) extends Table[Role](tag, "user_role") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")

  def * = (id.?, name) <> (Role.tupled, Role.unapply)
}

object RoleDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val roles = TableQuery[RoleTable]

  def getById(id: Long): Role = Await.result(db.run(roles.filter(_.id === id).result.head), 5.seconds)
  def listAll: List[Role] = Await.result(db.run(roles.to[List].result), 5.seconds)
}

/*
object Role {
	
	var roles = Set (
		Role(Some(1L), "admin"),
		Role(Some(2L), "support"),
		Role(Some(3L), "seller")
	)

	def getById(id: Long) = roles.find( _.id == id )
	def getByName(name: String) = roles.find( _.name == name )
	def listAll = roles.toList.sortBy(_.id)
}
*/ 