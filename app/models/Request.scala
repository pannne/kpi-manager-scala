package models

import java.sql.Timestamp
import java.sql.Date
import java.util.Calendar
import play.api.db.slick.DatabaseConfigProvider
import play.api.Play
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global
import play.api.Logger

case class Request(
  id: Option[Long],
  companyPIB: String,
  ownerId: Long,
  sellerId: Option[Long],
  statusId: Long,
  created: Option[Date],
  description: String,
  serviceQuantity: String
)

class RequestTable(tag: Tag) extends Table[Request](tag, "request") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def companyPIB = column[String]("company_pib")
  def ownerId = column[Long]("owner_id")
  def sellerId = column[Long]("seller_id")
  def statusId = column[Long]("status_id")
  def creationDate = column[Date]("created_on")
  def description = column[String]("description")
  def serviceQuantity = column[String]("service_quantity")

  def fk_company = foreignKey("COMPANY_FK", companyPIB, CompanyDAO.companies)(_.pib)
  def fk_owner = foreignKey("OWNER_FK", ownerId, UserDAO.users)(_.id)
  //def fk_status = foreignKey("STATUS_FK", statusId, RequestStatus)

  def * = (id.?, companyPIB, ownerId, sellerId.?, statusId, creationDate.?, description, serviceQuantity) <> (Request.tupled, Request.unapply)
}

object RequestDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val requests = TableQuery[RequestTable]

  private def filterById(id: Long): Query[RequestTable, Request, Seq] = requests.filter(_.id === id)
  private def filterByCompany(pib: String): Query[RequestTable, Request, List] = requests.filter(_.companyPIB === pib).to[List]
  private def filterByOwner(id: Long): Query[RequestTable, Request, List] = requests.filter(_.ownerId === id).to[List]
  private def filterBySeller(id: Long): Query[RequestTable, Request, List] = requests.filter(_.sellerId === id).to[List]
  private def filterByStatus(id: Long): Query[RequestTable, Request, List] = requests.filter(_.statusId === id).to[List]
  private def createRequestServiceRecords(requestId: Long, rec: String) = {
    // Update Service_request table

    val srvQList: Array[String] = rec.split(",")
    for (elem <- srvQList) {
      val tmp: Array[String] = elem.split("\\|")
      Logger.debug("SPLIT 2: " + elem + ", " + tmp(0) + ", " + tmp(1))
      // TODO:   BAD BAD BAD
      val entry = new RequestService(None, requestId, tmp(0).toInt, Some(tmp(2).toInt), Some(tmp(1).toInt))
      RequestServiceDAO.insert(entry)

    }
  }

  private def createHistoryRecords(requestId: Long): Unit = {
    val userHistoryRecord: UserHistory =
      new UserHistory(None, 1, "User created request: " + requestId, new java.sql.Date(Calendar.getInstance().getTime().getTime()))
    UserHistoryDAO.insert(userHistoryRecord)
    val requestHistoryRecord: RequestHistory = new RequestHistory(None, requestId, "Request created", new java.sql.Date(Calendar.getInstance().getTime().getTime()))
    RequestHistoryDAO.insert(requestHistoryRecord)
  }

  def getById(id: Int): Request = Await.result(db.run(filterById(id).result.head), 5.seconds)
  def getByCompany(pib: String): List[Request] = Await.result(db.run(filterByCompany(pib).result), 5.seconds)
  def getByOwner(id: Long): List[Request] = Await.result(db.run(filterByOwner(id).result), 5.seconds)
  def getBySeller(id: Long): List[Request] = Await.result(db.run(filterBySeller(id).result), 5.seconds)
  def getByStatus(id: Long): List[Request] = Await.result(db.run(filterByStatus(id).result), 5.seconds)
  def getAll: List[Request] = Await.result(db.run(requests.to[List].result), 5.seconds)

  def insertRequest(request: Request) = {
    val stupid = new Request(
      request.id,
      request.companyPIB,
      request.ownerId,
      request.sellerId,
      request.statusId,
      Some(new java.sql.Date(Calendar.getInstance().getTime().getTime())),
      request.description,
      request.serviceQuantity
    )
    val createdRequest: Request = Await.result(db.
      run(requests returning requests.map(_.id) += stupid).
      map(id => stupid.copy(id = Some(id))), 5.seconds)

    createRequestServiceRecords(createdRequest.id.get, createdRequest.serviceQuantity)
    createHistoryRecords(createdRequest.id.get)
  }

}

object RequestType {
  val types = List("Nov", "Seoba", "Obnova")
}
/*
object Request {
	
	var requests = Set (
		Request(Some(1L), Some(1L), Some("106475837"), Some(2L), Some(3L), Some(1L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "First request", Some(0), Some(" ")),
    Request(Some(2L), Some(1L), Some("100760118"), Some(1L), Some(3L), Some(1L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "2 request", Some(1), Some(" ")),
    Request(Some(3L), Some(5L), Some("100760118"), Some(1L), Some(3L), Some(3L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "3 request", Some(0), Some(" ")),
    Request(Some(4L), Some(4L), Some("100391986"), Some(1L), Some(3L), Some(4L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "4 request", Some(0), Some(" ")),
    Request(Some(5L), Some(2L), Some("102685624"), Some(2L), Some(3L), Some(5L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "5 request", Some(1), Some(" ")),
    Request(Some(6L), Some(5L), Some("106475837"), Some(2L), Some(3L), Some(2L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "6 request", Some(0), Some(" ")),
    Request(Some(7L), Some(5L), Some("109474676"), Some(1L), Some(3L), Some(3L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "7 request", Some(0), Some(" ")),
    Request(Some(8L), Some(3L), Some("100760118"), Some(2L), Some(3L), Some(1L),
      Some(new Date(Calendar.getInstance().getTimeInMillis())),
      "8 request", Some(0), Some(" "))
/*		Request(2, CompanyDAO.getById(1).value.get, 4L, 0, User.getById(3), User.getById(2), RequestStatus.getById(2),"", "request 2")
		Request(3, CompanyDAO.getById(2), 1L, 1, User.getById(3), User.getById(2), RequestStatus.getById(3),"", "request 3"),
		Request(4, CompanyDAO.getById(3), 2L, 1, User.getById(2), User.getById(2), RequestStatus.getById(4),"", "request 4"),
		Request(5, CompanyDAO.getById(4), 6L, 0, User.getById(2), User.getById(2), RequestStatus.getById(5),"", "request 5"),
		Request(6, CompanyDAO.getById(5), 5L, 0, User.getById(2), User.getById(2), RequestStatus.getById(1),"", "request 6"),
		Request(7, CompanyDAO.getById(6), 2L, 0, User.getById(2), User.getById(2), RequestStatus.getById(1),"", "request 7"),
		Request(8, CompanyDAO.getById(7), 1L, 0, User.getById(2), User.getById(2), RequestStatus.getById(2),"", "request 8"),
		Request(9, CompanyDAO.getById(8), 2L, 0, User.getById(2), User.getById(2), RequestStatus.getById(3),"", "request 9"),
		Request(10, CompanyDAO.getById(2), 3L, 0, User.getById(2), User.getById(2), RequestStatus.getById(2),"", "request 10"),
		Request(11, CompanyDAO.getById(4), 5L, 0, User.getById(2), User.getById(2), RequestStatus.getById(4),"", "request 11"),
		Request(12, CompanyDAO.getById(4), 6L, 0, User.getById(2), User.getById(2), RequestStatus.getById(1),"", "request 12")*/
	)

	def getById(id: Int) = requests.find(_.id == id)
	def getByCompany(pib: String) = requests.filter( _.companyPIB.get == pib).toList.sortWith( _.id.get > _.id.get)
	def getBySeller(id: Int) = requests.find(_.sellerId.get == id)
	def getBySupport(id: Int) = requests.find(_.ownerId == id)
	def getAll = requests.toList.sortWith( _.id.get > _.id.get)
	
}
*/
