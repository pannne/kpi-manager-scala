package models

import java.util.Calendar

import slick.driver.PostgresDriver.api._

import scala.concurrent.Future
import play.api.db.slick.DatabaseConfigProvider
import play.api.Play
import play.api.libs.json.Json
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile

import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

case class ServicePackage(id: Option[Long], name: String, description: Option[String])
case class BoxPackage(id: Option[Long], name: String, description: Option[String], pkgIds: Option[String])
case class BoxServicePackage(id: Option[Long], boxId: Long, packageId: Long)

class ServicePackageTable(tag: Tag) extends Table[ServicePackage](tag, "service_package") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def description = column[String]("description")

  def * = (id.?, name, description.?) <> (ServicePackage.tupled, ServicePackage.unapply)
}

class BoxPackageTable(tag: Tag) extends Table[BoxPackage](tag, "box_package") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name")
  def description = column[String]("description")
  def pkgIds = column[String]("package_ids")
  def * = (id.?, name, description.?, pkgIds.?) <> (BoxPackage.tupled, BoxPackage.unapply)
}

class BoxServicePackageTable(tag: Tag) extends Table[BoxServicePackage](tag, "box_service_package") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def boxId = column[Long]("box_package_id")
  def packageId = column[Long]("service_package_id")

  // Constraints
  def fkBox = foreignKey("BOX_FK", boxId, ServicePackageDAO.boxPackages)(_.id)
  def fkPackage = foreignKey("PACKAGE_FK", packageId, ServicePackageDAO.servicePackages)(_.id)

  def * = (id.?, boxId, packageId) <> (BoxServicePackage.tupled, BoxServicePackage.unapply)
}

object ServicePackageDAO {

  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val servicePackages = TableQuery[ServicePackageTable]
  val boxPackages = TableQuery[BoxPackageTable]
  val boxServicePackages = TableQuery[BoxServicePackageTable]

  private def filterById(id: Long): Query[ServicePackageTable, ServicePackage, Seq] =
    servicePackages.filter(_.id === id)

  private def getAllBoxPackagesQuery(boxId: Long): Query[BoxServicePackageTable, BoxServicePackage, List] =
    boxServicePackages.filter(_.boxId === boxId).to[List]

  def getAll: List[ServicePackage] = Await.result(db.run(servicePackages.to[List].result), 5.seconds)
  def getAllBoxPackages: List[BoxPackage] = Await.result(db.run(boxPackages.to[List].result), 5.seconds)
  def getAllBoxServicePackages: List[BoxServicePackage] = Await.result(db.run(boxServicePackages.to[List].result), 5.seconds)
  def getById(id: Long): ServicePackage = {
    Await.result(db.run(filterById(id).result.head), 5.seconds)
  }

  def createPackage(newPackage: ServicePackage): Future[Int] = db.run(servicePackages += newPackage)

  def createBox(newBox: BoxPackage) = {
    val createdBox: BoxPackage = Await.result(db.
      run(boxPackages returning boxPackages.map(_.id) += newBox).
      map(id => newBox.copy(id = Some(id))), 5.seconds)

    // Update linked table
    val packageIds = createdBox.pkgIds.getOrElse("")
    if (packageIds.nonEmpty) {
      val ids: Array[String] = packageIds.split(",")
      for (id <- ids) {
        try {
          val idNum = id.toInt
          val entry = new BoxServicePackage(None, createdBox.id.get, idNum)
          addboxPackageEntry(entry)
        } catch {
          case e: Exception => -1
        }
      }
    }
    val historyRecord: UserHistory = new UserHistory(None, 1, "New box package " + createdBox.name + " created", new java.sql.Date(Calendar.getInstance().getTime().getTime()))
    UserHistoryDAO.insert(historyRecord)
  }

  def getAllBoxPackages(boxId: Long): List[BoxServicePackage] = Await.result(db.run(getAllBoxPackagesQuery(boxId).result), 5.seconds)
  def addboxPackageEntry(entry: BoxServicePackage) = Await.result(db.run(boxServicePackages += entry), 5.seconds)

  implicit val boxServicePackageFormat = Json.format[BoxServicePackage]
  def getBoxPackageJson(): String = Json.obj("boxPackage" -> getAllBoxServicePackages).toString()

  implicit val packageFormat = Json.format[ServicePackage]
  def getPackageJson(): String = Json.obj("packages" -> getAll).toString()
}

/*
object ServicePackage {

	var sPackages = Set (
		ServicePackage(Some(1L), "BIZ paket"),
		ServicePackage(Some(2L), "Fixne usluge"),
		ServicePackage(Some(3L), "ADSL"),
		ServicePackage(Some(4L), "IPTV"),
		ServicePackage(Some(5L), "Mobilne usluge"),
		ServicePackage(Some(6L), "Mobilni internet")
	)

	def getAll = sPackages.toList.sortBy(_.id)
	def getById(id: Long) = sPackages.find(_.id.getOrElse(0) == id).get
}
*/ 