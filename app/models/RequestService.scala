package models

import play.api.db.slick.DatabaseConfigProvider
import play.api.Play
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile
import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

/**
 * Created by darko on 7/5/17.
 */
case class RequestService(id: Option[Long], requestId: Long, serviceId: Long, boxRequestId: Option[Long], quantity: Option[Int])

class RequestServiceTable(tag: Tag) extends Table[RequestService](tag, "service_request") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def requestId = column[Long]("request_id")
  def serviceId = column[Long]("service_id")
  def boxRequest = column[Long]("box_request_id")
  def quantity = column[Int]("quantity")

  // Constraints
  //def fkReq = foreignKey("REQ_FK", requestId, RequestDAO.services)(_.id)
  def fkSer = foreignKey("SRV_FK", serviceId, ServiceDAO.services)(_.id)

  def * = (id.?, requestId, serviceId, boxRequest.?, quantity.?) <> (RequestService.tupled, RequestService.unapply)
}

object RequestServiceDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val requestServices = TableQuery[RequestServiceTable]

  def insert(entry: RequestService) = Await.result(db.run(requestServices += entry), 5.seconds)
  def getByRequest(reqId: Long): List[RequestService] =
    Await.result(db.run(requestServices.filter(_.requestId === reqId).to[List].result), 5.seconds)
}