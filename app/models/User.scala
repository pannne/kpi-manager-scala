package models

import java.sql.Date

import play.api.db.slick.DatabaseConfigProvider
import play.api.{ Logger, Play }
import slick.driver.PostgresDriver.api._
import slick.driver.JdbcProfile

import scala.concurrent.Future
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import utils.silhouette._

import scala.concurrent.ExecutionContext.Implicits.global

case class User(
    id: Option[Long],
    username: String,
    password: String,
    email: String,
    createdBy: Option[String],
    createdOn: Option[Date],
    lastLogin: Option[Date],
    roleId: Long,
    activeUser: Option[Boolean],
    emailConfirmed: Boolean,
    firstName: String,
    lastName: String
) extends IdentitySilhouette {
  def key = email
  def fullName: String = firstName + " " + lastName
}

class UserTable(tag: Tag) extends Table[User](tag, "user_table") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def username = column[String]("username")
  def password = column[String]("password")
  def email = column[String]("email")
  def createdBy = column[String]("created_by")
  def createdOn = column[Date]("created_on")
  def lastLogin = column[Date]("last_login")
  def roleId = column[Long]("role_id")
  def activeUser = column[Boolean]("user_active")
  def emailConfirmed = column[Boolean]("email_confirmed")
  def firstName = column[String]("first_name")
  def lastName = column[String]("last_name")

  // Constraints
  def fk_role_id = foreignKey("ROLE_FK", roleId, RoleDAO.roles)(_.id)

  def * = (id.?, username, password, email, createdBy.?,
    createdOn.?, lastLogin.?, roleId, activeUser.?, emailConfirmed, firstName, lastName) <> (User.tupled, User.unapply)
}

object UserDAO {
  private val dbConfig = DatabaseConfigProvider.get[JdbcProfile](Play.current)
  private val db = dbConfig.db
  val users = TableQuery[UserTable]

  private def filterById(id: Long): Query[UserTable, User, Seq] = users.filter(_.id === id)
  private def filterByName(name: String): Query[UserTable, User, Seq] = users.filter(_.username === name)
  private def filterByMail(email: String): Query[UserTable, User, Seq] = users.filter(_.email === email)

  def getById(id: Long): User = Await.result(db.run(filterById(id).result.head), 5.seconds)
  def getByUsername(name: String): User = Await.result(db.run(filterByName(name).result.head), 5.seconds)
  def getAll: List[User] = Await.result(db.run(users.to[List].result), 5.seconds).sortBy(_.id)
  def addUser(newUser: User): Future[Int] = db.run(users += newUser)

  def findByEmail(email: String): Future[Option[User]] = db.run(filterByMail(email).result.headOption)
  def findUserByMail(mail: String): User = Await.result(db.run(filterByMail(mail).result.head), 5.seconds)

  def remove(email: String): Future[Unit] = Future.successful()
  def save(user: User): Future[User] = {
    Logger.logger.debug("NEW USER PASSWORD: " + user.password);
    findByEmail(user.email).map {
      case Some(existingUser) => {
        Logger.logger.debug("Update users credentials : " + user.email)
        // Update password and email confirmed.

        val action = filterByMail(user.email).map(found => (found.password, found.emailConfirmed)).
          update(user.password, true)
        db.run(action)
        user
      }
      case _ => {
        Logger.logger.debug("No user found, create new one : " + user.email)

        addUser(user)
        user
      }
    }
  }
}

/*
		User(Some(2L), "paki", "bbbb", Some("paki@pianolocco.com"), Role(Some(2L), "support")),
		User(Some(3L), "doca", "cccc", Some("doca@pianolocco.com"), Role(Some(3L), "seller"))
	)

	def getById(id: Long) = users.find(_.id == Some(id)).get
	def getByUsername(name: String ) = users.find(_.username == name)
	def getAll = users.toList.sortBy(_.id)
	def addUser(newUser: User) {
		val newUserRole: Role = Role.getByName(newUser.role.name).get
		val copiedUser: User = User(newUser.id, newUser.username, newUser.pass, newUser.email, newUserRole)
		users = users + copiedUser 
	}
}
*/ 