package models

case class RequestStatus(id: Int, name: String)

object RequestStatus {

  var statuses = Set(
    RequestStatus(1, "U radu"),
    RequestStatus(2, "Realizovan"),
    RequestStatus(3, "Ceka ugovor"),
    RequestStatus(4, "Odustao"),
    RequestStatus(5, "Sporan")
  )

  def getById(id: Int) = statuses.find(_.id == id).get
  def getAll = statuses.toList.sortBy(_.id)

}
