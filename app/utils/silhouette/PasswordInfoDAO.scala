package utils.silhouette

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.util.PasswordInfo
import com.mohiva.play.silhouette.persistence.daos.DelegableAuthInfoDAO
import models.{ User, UserDAO }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import Implicits._
import play.api.Logger

class PasswordInfoDAO extends DelegableAuthInfoDAO[PasswordInfo] {

  def add(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    update(loginInfo, authInfo)

  def find(loginInfo: LoginInfo): Future[Option[PasswordInfo]] =
    UserDAO.findByEmail(loginInfo.providerKey).map {
      case Some(user) if user.emailConfirmed => Some(user.password)
      case _ => None
    }

  def remove(loginInfo: LoginInfo): Future[Unit] = UserDAO.remove(loginInfo.providerKey)

  def save(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] =
    find(loginInfo).flatMap {
      case Some(_) => update(loginInfo, authInfo)
      case None => add(loginInfo, authInfo)
    }

  def update(loginInfo: LoginInfo, authInfo: PasswordInfo): Future[PasswordInfo] = {
    val potentialUser = UserDAO.findByEmail(loginInfo.providerKey)
    Logger.logger.debug("Found user: " + potentialUser)
    potentialUser.map {
      case Some(user) => {
        UserDAO.save(user.copy(password = authInfo.password))
        authInfo
      }
      case _ => {

        throw new Exception("PasswordInfoDAO - update : the user must exists to update its password")
      }
    }
  }

}