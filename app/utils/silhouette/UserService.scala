package utils.silhouette

import com.mohiva.play.silhouette.api.LoginInfo
import com.mohiva.play.silhouette.api.services.IdentityService
import models.{ User, UserDAO }

import scala.concurrent.Future

class UserService extends IdentityService[User] {
  def retrieve(loginInfo: LoginInfo): Future[Option[User]] = UserDAO.findByEmail(loginInfo.providerKey)
  def save(user: User): Future[User] = UserDAO.save(user)
}