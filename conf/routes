# Routes
# This file defines all application routes (Higher priority routes first)
# ~~~~

# An example controller showing a sample home page
# GET     /                           controllers.HomeController.index
# An example controller showing how to use dependency injection
GET     /count                      controllers.CountController.count
# An example controller showing how to write asynchronous code
GET     /message                    controllers.AsyncController.message

# Map static resources from the /public folder to the /assets URL path
GET     /assets/*file                   controllers.Assets.versioned(path="/public", file: Asset)

GET 	/companies 		                controllers.CompanyController.listAll
POST 	/companies		                controllers.CompanyController.save
GET 	/company/new                    controllers.CompanyController.newCompany
GET     /companies/filter/:pib          controllers.CompanyController.filter(pib)

GET	    /requests		                controllers.RequestsController.listAll
GET     /requests/user                  controllers.RequestsController.listUsersRequests()
GET     /requests/filter/:filterStr     controllers.RequestsController.filter(filterStr)
GET     /request/new/:packageId         controllers.RequestsController.newRequest(packageId: Long)
GET     /request/newBox/:boxId          controllers.RequestsController.newBoxRequest(boxId: Long)
GET     /requestDetails/:id             controllers.RequestsController.details(id: Long)
POST    /request/new/:packageId         controllers.RequestsController.create(packageId: Long)

GET 	/users			                controllers.UserController.listAll
GET 	/users/:name		            controllers.UserController.showDetails(name)
POST	/users			                controllers.UserController.save
GET	    /user/new		                controllers.UserController.newUser

# GET     /services/:packageId           controllers.ServicePackageController.listServices(packageId: Long)
GET     /services/:id                    controllers.ServicePackageController.serviceDetails(id: Long)
PUT     /services/:id                    controllers.ServicePackageController.serviceUpdate(id: Long)
GET     /service/new                     controllers.ServicePackageController.newService()
POST    /service                         controllers.ServicePackageController.createService()

GET     /servicePackages/:id            controllers.ServicePackageController.showDetails(id: Long)
GET     /servicePackage/new             controllers.ServicePackageController.newPackage()
POST    /servicePackage                 controllers.ServicePackageController.createPackage()

GET     /boxPackage/new                 controllers.ServicePackageController.newBox()
POST    /boxPackage                     controllers.ServicePackageController.createBox()

# Authentication pages
GET		/signup									controllers.Auth.startSignUp
POST	/signup									controllers.Auth.handleStartSignUp
GET		/signup/:token					controllers.Auth.signUp(token: String)
GET		/signin									controllers.Auth.signIn
POST	/authenticate						controllers.Auth.authenticate
GET		/signout								controllers.Auth.signOut
GET		/reset-password					controllers.Auth.forgotPassword
POST	/reset-password					controllers.Auth.handleForgotPassword
GET		/reset-password/:token	controllers.Auth.resetPassword(token: String)
POST	/reset-password/:token	controllers.Auth.handleResetPassword(token: String)
GET		/change-password				controllers.Auth.changePassword
POST	/change-password				controllers.Auth.handleChangePassword


# Home page
GET		/												controllers.Application.index
GET		/serviceA					controllers.Application.serviceSupport
GET		/serviceA_or_ServiceB		controllers.Application.serviceSeller
#GET		/serviceA_and_ServiceB	controllers.Application.serviceAandServiceB
GET		/settings								controllers.Application.serviceAdmin
GET		/myaccount							controllers.Application.myAccount

GET		/lang/$lang<(en|es)>		controllers.Application.selectLang(lang: String)

# Map static resources from the /public folder to the /assets URL path
GET		/public/*file						controllers.MyAssets.public(path="/public", file: Asset)
GET		/lib/*file							controllers.MyAssets.lib(path="/public/lib", file: Asset)
GET		/css/*file							controllers.MyAssets.css(path="/public/stylesheets", file: Asset)
GET		/js/*file								controllers.MyAssets.js(path="/public/javascripts", file: Asset)
GET		/img/*file							controllers.MyAssets.img(path="/public/images", file: Asset)
GET		/common/css/*file				controllers.MyAssets.commonCss(path="/public/lib/common/stylesheets", file: Asset)
GET		/common/js/*file				controllers.MyAssets.commonJs(path="/public/lib/common/javascripts", file: Asset)
GET		/common/img/*file				controllers.MyAssets.commonImg(path="/public/lib/common/images", file: Asset)