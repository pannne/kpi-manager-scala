# --- !Ups
CREATE TABLE company (
	id serial PRIMARY KEY,
	pib VARCHAR(9) UNIQUE NOT NULL,
	name VARCHAR(255) NOT NULL,
	tis_id VARCHAR(30),
	zr VARCHAR(255),
	registered BOOLEAN NOT NULL DEFAULT FALSE,
	fraud INTEGER DEFAULT 0,
	valid BOOLEAN NOT NULL DEFAULT TRUE
);

CREATE TABLE user_role (
	id serial PRIMARY KEY,
	name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE user_table (
	id serial PRIMARY KEY,
	username VARCHAR(255) UNIQUE NOT NULL,
	password VARCHAR(255) NOT NULL,
	email VARCHAR(255) UNIQUE NOT NULL,
	created_by VARCHAR(255),
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	last_login TIMESTAMP,
	role_id INTEGER,
	user_active BOOLEAN DEFAULT TRUE,
	email_confirmed BOOLEAN DEFAULT FALSE,
	first_name VARCHAR(255) NOT NULL,
	last_name VARCHAR(255) NOT NULL,
	FOREIGN KEY(role_id) REFERENCES user_role(id)
);

CREATE TABLE request_status (
	id serial PRIMARY KEY,
	name VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE request_type(
    type VARCHAR(255) UNIQUE NOT NULL
);

CREATE TABLE box_package(
    id serial PRIMARY KEY,
    name VARCHAR(255) UNIQUE NOT NULL,
    description VARCHAR,
    package_ids VARCHAR(255)
);

CREATE TABLE service_package(
	id serial PRIMARY KEY,
	name VARCHAR(255) UNIQUE NOT NULL,
	description VARCHAR
);

CREATE TABLE box_service_package(
    id serial PRIMARY KEY,
    box_package_id INTEGER,
    service_package_id INTEGER,
    FOREIGN KEY(box_package_id) REFERENCES box_package(id),
    FOREIGN KEY(service_package_id) REFERENCES service_package(id)
);

CREATE TABLE service(
	id serial PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	service_package_id INTEGER,
	description VARCHAR,
	FOREIGN KEY(service_package_id) REFERENCES service_package(id)
); 

CREATE TABLE request(
	id serial PRIMARY KEY,
	company_pib VARCHAR(20),
	owner_id INTEGER,
	seller_id INTEGER NOT NULL,
	status_id INTEGER,
	type VARCHAR(255) NOT NULL,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	description VARCHAR,
	service_quantity VARCHAR,
	FOREIGN KEY(company_pib) REFERENCES company(pib),
	FOREIGN KEY(owner_id) REFERENCES user_table(id),
	FOREIGN KEY(status_id) REFERENCES request_status(id),
	FOREIGN KEY(type) REFERENCES request_type(type)
);

CREATE TABLE request_history(
	id serial PRIMARY KEY,
	request_id INTEGER,
	user_id INTEGER,
	comment VARCHAR,
	created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	incident_number VARCHAR,
	user_generated BOOLEAN DEFAULT FALSE,
	FOREIGN KEY(request_id) REFERENCES request(id)
);

CREATE TABLE user_history(
    id serial PRIMARY KEY,
    user_id INTEGER,
    comment VARCHAR,
    created_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY(user_id) REFERENCES user_table(id)
);

CREATE TABLE service_request(
	id serial PRIMARY KEY,
	request_id INTEGER NOT NULL,
	service_id INTEGER  NOT NULL,
	box_request_id INTEGER,
	quantity INTEGER DEFAULT 0,
	FOREIGN KEY(request_id) REFERENCES request(id),
	FOREIGN KEY(service_id) REFERENCES service(id)
);

INSERT INTO company(pib, name, tis_id)
VALUES
( '106475837', 'EXPERIENS PLUS DOO BEOGRAD', '5453254'),
( '100760118', 'PODMLADAK DOO', '2926281'),
( '100391986', 'OMLADINSKI KAMP ĐERDAP', '847266'),
( '102685624', 'ASTRA COOL', '3027139'),
( '104031026', 'DRVOIMPEX INŽENJERING', '5557278'),
( '102201098', 'MNEM SYNE', '891829'),
( '109474676', 'FURNIX', '20250472'),
( '108852950', 'CABLENETWORK ENGINEERING', '');	

INSERT INTO user_role(name)
VALUES
('system'),
('admin'),
('support'),
('seller');

INSERT INTO user_table(username, password, email, role_id, email_confirmed, first_name, last_name)
VALUES
('su', '$2a$10$HqRIHR3owqAL1j/jbgS2juZesttSLZOlNMXwEG0raI7sr9uWQ/zGC', 'system@user.rs', 1, true, 'system', 'user');

INSERT INTO request_status(name)
VALUES
('U radu'),
('Realizovan'),
('Ceka ugovor'),
('Odustao'),
('Sporan');

INSERT INTO service_package(name, description)
VALUES
('Fixne usluge', 'Fixne usluge'),
('ADSL', 'ADSL'),
('IPTV', 'IPTV'),
('Mobilne usluge', 'MOB'),
('Mobilni internet', 'MOB NET');

INSERT INTO service(name, service_package_id, description)
VALUES
('Nov POTS', 1, 'Nov POTS'),
('Nov BBTF', 1, 'Nov BBTF'),
('Postojeca linija', 1, 'Postojeca linija'),
('Ustupanje', 1, 'Ustupanje'),
('Zamena ISDN', 1, 'Zamena ISDN'),
('PK/PB', 2, 'PK/PB'),
('Statika', 2, 'Statika'),
('DEM', 2, 'DEM'),
('Tablet', 2, 'Tablet'),
('DEM', 3, 'DEM');

INSERT INTO request_type(type)
VALUES
('Nov'),
('Seoba'),
('Obnova');

# --- !Downs

DROP TABLE IF EXISTS request_history;
DROP TABLE IF EXISTS service_request;
DROP TABLE IF EXISTS request;
DROP TABLE IF EXISTS request_type;
DROP TABLE IF EXISTS company;
DROP TABLE IF EXISTS user_history;
DROP TABLE IF EXISTS user_table;
DROP TABLE IF EXISTS user_role;
DROP TABLE IF EXISTS request_status;
DROP TABLE IF EXISTS service;
DROP TABLE IF EXISTS box_service_package;
DROP TABLE IF EXISTS service_package;
DROP TABLE IF EXISTS box_package;

